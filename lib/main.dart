import 'package:birdgang_repository/widgets/align/align.dart';
import 'package:birdgang_repository/widgets/backdrop_filter/backdrop_filter.dart';
import 'package:flutter/material.dart';

void main() => runApp(BackdropFilterPage());