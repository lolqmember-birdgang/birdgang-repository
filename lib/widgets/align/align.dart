import 'package:flutter/material.dart';


/*
 * https://api.flutter.dev/flutter/widgets/Align-class.html
 */
class AlignPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: AlignPositionBody(),
      ),
    );
  }
}


class AlignTopRightBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 120.0,
        width: 120.0,
        color: Colors.blue[50],
        child: Align(
          alignment: Alignment.topRight,
          child: FlutterLogo(
            size: 60,
          ),
        ),
      ),
    );
  }
}


class AlignPositionBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 120.0,
        width: 120.0,
        color: Colors.blue[50],
        child: Align(
          alignment: Alignment(0, -1),
          child: FlutterLogo(
            size: 60,
          ),
        ),
      ),
    );
  }
}