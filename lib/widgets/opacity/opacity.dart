import 'package:flutter/material.dart';

/*
 * https://api.flutter.dev/flutter/widgets/Opacity-class.html
 */
class OpacityPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: OpacityBody(),
    );
  }
}

class OpacityBody extends StatefulWidget {
  @override
  _OpacityBodyState createState() {
    return _OpacityBodyState();
  }
}

class _OpacityBodyState extends State<OpacityBody> {
  var _visible = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container (
        alignment: FractionalOffset.center,
        child: Opacity(
          opacity: _visible ? 1.0 : 0.0,
          child: const Text('Now you see me, now you don\'t!',),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            setState(() {
              _visible = false;
            });
          },
          label: Text("Hide Button")
      ),
    );
  }

}