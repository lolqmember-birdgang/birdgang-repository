import 'package:flutter/material.dart';

/*
 * https://api.flutter.dev/flutter/widgets/AnimatedContainer-class.html
 */
class AnimatedContainerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AnimatedContainerBody(),
    );
  }
}


class AnimatedContainerBody extends StatefulWidget {
  @override
  _AnimatedContainerBodyState createState() {
    return _AnimatedContainerBodyState();
  }
}


class _AnimatedContainerBodyState extends State<AnimatedContainerBody> {

  var _alignment = Alignment.bottomCenter;
  var color = Colors.yellow;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        padding: EdgeInsets.all(10.0),
        duration: Duration(seconds: 1),
        alignment: _alignment,
        color: color,
        child: Container(
          child: Icon(Icons.airplanemode_active, size: 50.0, color: Colors.yellow,),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.blueAccent,
          onPressed: (){
            setState(() {
              _alignment = Alignment.center;
              color = Colors.blue;
            });
          },
          icon: Icon(Icons.airplanemode_active),
          label: Text("Take Flight")
      ),
    );
  }
}



class GradientTransform extends StatefulWidget {
  @override
  GradientTransformState createState() {
    return GradientTransformState();
  }
}

class GradientTransformState extends State<GradientTransform> {

  var top = FractionalOffset.topCenter;
  var bottom = FractionalOffset.bottomCenter;
  var list = [
    Colors.lightGreen,
    Colors.redAccent,
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: AnimatedContainer(
            height: 300.0,
            width: 300.0,
            duration: Duration(seconds: 1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                gradient: LinearGradient(
                  begin: top,
                  end: bottom,
                  colors: list,
                  stops: [0.0, 1.0],
                ),
                color: Colors.lightGreen
            ),
          ),
        ),

        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
            onPressed: (){
              setState(() {
                top = FractionalOffset.bottomLeft;
                bottom = FractionalOffset.topRight;
                list = [
                  Colors.blueAccent, Colors.yellowAccent
                ];
              });
            },
            icon: Icon(Icons.update),
            label: Text("Transform")),
      ),
    );
  }
}
