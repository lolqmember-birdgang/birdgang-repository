import 'package:flutter/material.dart';


/*
 * https://api.flutter.dev/flutter/widgets/Expanded-class.html
 */
class ExpandedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ExpandedBody(),
      ),
    );
  }
}


class ExpandedBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              color: Colors.red,
              height: 100,
            ),
          ),
          Container(
            color: Colors.blue,
            height: 100,
            width: 50,
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.red,
              height: 100,
            ),
          ),
        ],
      ),
    );
  }
}