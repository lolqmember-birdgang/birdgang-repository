import 'package:flutter/material.dart';


/*
 * https://api.flutter.dev/flutter/widgets/SafeArea-class.html
 */
class SafeAreaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: BodyWidgetWithSafeArea(),
      ),
    );
  }
}


class BodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        "A widget that insets its child by sufficient padding to avoid intrusions by the operating system." +
        "For example, this will indent the child by enough to avoid the status bar at the top of the screen." +
        "It will also indent the child by the amount necessary to avoid The Notch on the iPhone X, or other similar creative physical features of the display."
      ),
    );
  }
}


class BodyWidgetWithSafeArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: SafeArea(
        left: true,
        top: true,
        right: true,
        bottom: true,
        child: Text(
          "A widget that insets its child by sufficient padding to avoid intrusions by the operating system." +
          "For example, this will indent the child by enough to avoid the status bar at the top of the screen." +
          "It will also indent the child by the amount necessary to avoid The Notch on the iPhone X, or other similar creative physical features of the display."
        ),
      ),
    );
  }

}