import 'package:flutter/material.dart';


/*
 * https://api.flutter.dev/flutter/widgets/Table-class.html
 */
class TablePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: TableBody(),
      ),
    );
  }
}


class TableBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.grey),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(color: Colors.red),
                  child: Text("item 1"),
                ),
                Container(
                  decoration: BoxDecoration(color: Colors.amber),
                  child: Text("item 3"),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(color: Colors.green),
                  child: Text("item 2"),
                ),
                Container(
                  decoration: BoxDecoration(color: Colors.teal),
                  child: Text("item 4"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}